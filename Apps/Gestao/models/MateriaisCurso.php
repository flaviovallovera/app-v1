<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "materiaiscurso".
 *
 * @property int $id
 * @property int $cursoid
 * @property string $descricao
 * @property string $link
 * @property int $ativo
 */
class MateriaisCurso extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'materiaiscurso';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cursoid', 'ativo'], 'integer'],
            [['descricao'], 'string', 'max' => 50],
            [['link'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cursoid' => 'Cursoid',
            'descricao' => 'Descricao',
            'link' => 'Link',
            'ativo' => 'Ativo',
        ];
    }

    /**
     * {@inheritdoc}
     * @return MateriaisCursoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MateriaisCursoQuery(get_called_class());
    }
}
