<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "configuracao_site".
 *
 * @property int $id
 * @property string $campo
 * @property string $valor
 */
class ConfiguracaoSite extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'configuracao_site';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['campo', 'valor'], 'required'],
            [['valor'], 'string'],
            [['campo'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'campo' => 'Campo',
            'valor' => 'Valor',
        ];
    }

    /**
     * {@inheritdoc}
     * @return ConfiguracaoSiteQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ConfiguracaoSiteQuery(get_called_class());
    }
}
