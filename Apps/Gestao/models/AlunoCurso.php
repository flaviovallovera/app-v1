<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alunocurso".
 *
 * @property int $id
 * @property int|null $aluno
 * @property string|null $dataCadastro
 * @property int|null $curso
 * @property int|null $possuiCertificado
 * @property int|null $Status 1- ativo e pago 2- cancelado 3-aguardando pagamento
 * @property string|null $dataFinalizacao
 * @property int|null $turmaId
 */
class AlunoCurso extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alunocurso';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['aluno', 'curso', 'possuiCertificado', 'Status', 'turmaId'], 'integer'],
            [['dataCadastro', 'dataFinalizacao'], 'safe'],
            [['aluno', 'curso', 'turmaId', 'Status'], 'unique', 'targetAttribute' => ['aluno', 'curso', 'turmaId', 'Status']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'aluno' => 'Aluno',
            'dataCadastro' => 'Data Cadastro',
            'curso' => 'Curso',
            'possuiCertificado' => 'Possui Certificado',
            'Status' => '1- ativo e pago
2- cancelado
3-aguardando pagamento',
            'dataFinalizacao' => 'Data Finalizacao',
            'turmaId' => 'Turma ID',
        ];
    }

    /**
     * {@inheritdoc}
     * @return AlunoCursoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AlunoCursoQuery(get_called_class());
    }
}
