<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TransacaoEnderecoCobranca;

/**
 * TransacaoEnderecoCobrancaSearch represents the model behind the search form of `app\models\TransacaoEnderecoCobranca`.
 */
class TransacaoEnderecoCobrancaSearch extends TransacaoEnderecoCobranca
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idTransacao'], 'integer'],
            [['endereco', 'numero', 'complemento', 'bairro', 'cidade', 'estado', 'pais', 'cep'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TransacaoEnderecoCobranca::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idTransacao' => $this->idTransacao,
        ]);

        $query->andFilterWhere(['like', 'endereco', $this->endereco])
            ->andFilterWhere(['like', 'numero', $this->numero])
            ->andFilterWhere(['like', 'complemento', $this->complemento])
            ->andFilterWhere(['like', 'bairro', $this->bairro])
            ->andFilterWhere(['like', 'cidade', $this->cidade])
            ->andFilterWhere(['like', 'estado', $this->estado])
            ->andFilterWhere(['like', 'pais', $this->pais])
            ->andFilterWhere(['like', 'cep', $this->cep]);

        return $dataProvider;
    }
}
