<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "turma".
 *
 * @property int $id
 * @property string $descricao
 * @property string $dataInicio
 * @property string $dataFimInscricao
 * @property int $ativo
 * @property int $cursoId
 * @property string|null $nomeContrato
 * @property string $cidade
 * @property float $descontoValor
 * @property int $vagas
 */
class Turma extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'turma';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dataInicio', 'dataFimInscricao'], 'safe'],
            [['ativo', 'cursoId', 'vagas'], 'integer'],
            [['descontoValor'], 'number'],
            [['descricao', 'cidade'], 'string', 'max' => 50],
            [['nomeContrato'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descricao' => 'Descricao',
            'dataInicio' => 'Data Inicio',
            'dataFimInscricao' => 'Data Fim Inscricao',
            'ativo' => 'Ativo',
            'cursoId' => 'Curso ID',
            'nomeContrato' => 'Nome Contrato',
            'cidade' => 'Cidade',
            'descontoValor' => 'Desconto Valor',
            'vagas' => 'Vagas',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TurmaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TurmaQuery(get_called_class());
    }
}
