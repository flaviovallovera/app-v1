<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TransacaoEnderecoCobranca]].
 *
 * @see TransacaoEnderecoCobranca
 */
class TransacaoEnderecoCobrancaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return TransacaoEnderecoCobranca[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return TransacaoEnderecoCobranca|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
