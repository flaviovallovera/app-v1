<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "email".
 *
 * @property int $Id
 * @property string|null $SmtpServer
 * @property int|null $SmtpPort
 * @property string|null $Username
 * @property string|null $loPassword
 * @property string|null $PassPhrase
 * @property int|null $EnableSsl
 * @property string|null $Subject
 * @property string|null $Body
 * @property string|null $loFrom
 * @property string|null $ReplyTo
 * @property string|null $loTo
 * @property string|null $Cc
 * @property string|null $Cco
 * @property int|null $IsHtml
 * @property int|null $loStatus
 * @property string|null $CreateDate
 * @property string|null $SentDate
 */
class Email extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'email';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SmtpPort', 'EnableSsl', 'IsHtml', 'loStatus'], 'integer'],
            [['Body'], 'string'],
            [['CreateDate', 'SentDate'], 'safe'],
            [['SmtpServer'], 'string', 'max' => 200],
            [['Username', 'loPassword', 'loFrom', 'ReplyTo', 'loTo', 'Cc', 'Cco'], 'string', 'max' => 50],
            [['PassPhrase'], 'string', 'max' => 100],
            [['Subject'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'SmtpServer' => 'Smtp Server',
            'SmtpPort' => 'Smtp Port',
            'Username' => 'Username',
            'loPassword' => 'Lo Password',
            'PassPhrase' => 'Pass Phrase',
            'EnableSsl' => 'Enable Ssl',
            'Subject' => 'Subject',
            'Body' => 'Body',
            'loFrom' => 'Lo From',
            'ReplyTo' => 'Reply To',
            'loTo' => 'Lo To',
            'Cc' => 'Cc',
            'Cco' => 'Cco',
            'IsHtml' => 'Is Html',
            'loStatus' => 'Lo Status',
            'CreateDate' => 'Create Date',
            'SentDate' => 'Sent Date',
        ];
    }

    /**
     * {@inheritdoc}
     * @return EmailQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EmailQuery(get_called_class());
    }
}
