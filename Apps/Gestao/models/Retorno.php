<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "retorno".
 *
 * @property int $id
 * @property string|null $id_transacao
 * @property float|null $valor
 * @property string|null $num_pedido
 * @property string|null $status_pagamento
 * @property string|null $mensagem_transacao
 * @property string|null $metodo
 * @property string|null $operadora
 * @property string|null $operadora_mensagem
 * @property string|null $id_librepag
 * @property string|null $autorizacao_id
 * @property string|null $url_autenticacao
 * @property string|null $dataAtualizacao
 */
class Retorno extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'retorno';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['valor'], 'number'],
            [['status_pagamento'], 'string'],
            [['dataAtualizacao'], 'safe'],
            [['id_transacao', 'num_pedido'], 'string', 'max' => 50],
            [['mensagem_transacao', 'url_autenticacao'], 'string', 'max' => 300],
            [['metodo', 'operadora', 'id_librepag', 'autorizacao_id'], 'string', 'max' => 30],
            [['operadora_mensagem'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_transacao' => 'Id Transacao',
            'valor' => 'Valor',
            'num_pedido' => 'Num Pedido',
            'status_pagamento' => 'Status Pagamento',
            'mensagem_transacao' => 'Mensagem Transacao',
            'metodo' => 'Metodo',
            'operadora' => 'Operadora',
            'operadora_mensagem' => 'Operadora Mensagem',
            'id_librepag' => 'Id Librepag',
            'autorizacao_id' => 'Autorizacao ID',
            'url_autenticacao' => 'Url Autenticacao',
            'dataAtualizacao' => 'Data Atualizacao',
        ];
    }

    /**
     * {@inheritdoc}
     * @return RetornoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RetornoQuery(get_called_class());
    }
}
