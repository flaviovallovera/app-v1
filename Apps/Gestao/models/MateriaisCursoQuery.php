<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[MateriaisCurso]].
 *
 * @see MateriaisCurso
 */
class MateriaisCursoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return MateriaisCurso[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return MateriaisCurso|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
