<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "curso".
 *
 * @property int $id
 * @property string|null $nome
 * @property string|null $descricao
 * @property string|null $cargaHoraria
 * @property float|null $valor
 * @property int|null $status
 * @property string|null $Colunatopico1
 * @property string|null $Colunatopico2
 * @property string|null $Colunatopico3
 * @property string|null $Colunatopico4
 * @property string|null $Colunatopico5
 * @property string|null $Colunatopico6
 * @property string|null $Colunatopico7
 * @property string|null $Colunatopico8
 * @property string|null $Colunatopico9
 * @property string|null $Colunatopico10
 * @property string|null $Colunaconteudo1
 * @property string|null $Colunaconteudo2
 * @property string|null $Colunaconteudo3
 * @property string|null $Colunaconteudo4
 * @property string|null $Colunaconteudo5
 * @property string|null $Colunaconteudo6
 * @property string|null $Colunaconteudo7
 * @property string|null $Colunaconteudo8
 * @property string|null $Colunaconteudo9
 * @property string|null $Colunaconteudo10
 * @property int|null $parcelamentoMaximoCartao
 * @property float|null $valorNoBoleto
 * @property int|null $tipoCurso 1 - padrão 2 - turma especial
 */
class Curso extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'curso';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descricao', 'Colunaconteudo1', 'Colunaconteudo2', 'Colunaconteudo3', 'Colunaconteudo4', 'Colunaconteudo5', 'Colunaconteudo6', 'Colunaconteudo7', 'Colunaconteudo8', 'Colunaconteudo9', 'Colunaconteudo10'], 'string'],
            [['valor', 'valorNoBoleto'], 'number'],
            [['status', 'parcelamentoMaximoCartao', 'tipoCurso'], 'integer'],
            [['nome'], 'string', 'max' => 45],
            [['cargaHoraria'], 'string', 'max' => 50],
            [['Colunatopico1', 'Colunatopico2'], 'string', 'max' => 250],
            [['Colunatopico3', 'Colunatopico4', 'Colunatopico5', 'Colunatopico6', 'Colunatopico7', 'Colunatopico8', 'Colunatopico9', 'Colunatopico10'], 'string', 'max' => 240],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'descricao' => 'Descricao',
            'cargaHoraria' => 'Carga Horaria',
            'valor' => 'Valor',
            'status' => 'Status',
            'Colunatopico1' => 'Colunatopico1',
            'Colunatopico2' => 'Colunatopico2',
            'Colunatopico3' => 'Colunatopico3',
            'Colunatopico4' => 'Colunatopico4',
            'Colunatopico5' => 'Colunatopico5',
            'Colunatopico6' => 'Colunatopico6',
            'Colunatopico7' => 'Colunatopico7',
            'Colunatopico8' => 'Colunatopico8',
            'Colunatopico9' => 'Colunatopico9',
            'Colunatopico10' => 'Colunatopico10',
            'Colunaconteudo1' => 'Colunaconteudo1',
            'Colunaconteudo2' => 'Colunaconteudo2',
            'Colunaconteudo3' => 'Colunaconteudo3',
            'Colunaconteudo4' => 'Colunaconteudo4',
            'Colunaconteudo5' => 'Colunaconteudo5',
            'Colunaconteudo6' => 'Colunaconteudo6',
            'Colunaconteudo7' => 'Colunaconteudo7',
            'Colunaconteudo8' => 'Colunaconteudo8',
            'Colunaconteudo9' => 'Colunaconteudo9',
            'Colunaconteudo10' => 'Colunaconteudo10',
            'parcelamentoMaximoCartao' => 'Parcelamento Maximo Cartao',
            'valorNoBoleto' => 'Valor No Boleto',
            'tipoCurso' => '1 - padrão
2 - turma especial',
        ];
    }

    /**
     * {@inheritdoc}
     * @return CursoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CursoQuery(get_called_class());
    }
}
