<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Transacao;

/**
 * TransacaoSearch represents the model behind the search form of `app\models\Transacao`.
 */
class TransacaoSearch extends Transacao
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'alunoId', 'alunoCursoId', 'descontoAlunoId', 'parcelas', 'status_pagamento', 'status'], 'integer'],
            [['cpfAluno', ' id_transacao', 'metodoPagamento', 'pedido', 'mensagem_transacao', 'metodo', 'operadora', 'operadora_mensagem', 'id_librepag', 'autorizacao_id', 'url_autenticacao', 'cvv_cartao', 'nome_cartao', 'num_cartao', 'mes_cartao', 'ano_cartao', 'ip', 'dataTransacao', 'dataAtualizacao', 'retornoIPAG'], 'safe'],
            [['valor', 'valorPago', 'desconto'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Transacao::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'alunoId' => $this->alunoId,
            'alunoCursoId' => $this->alunoCursoId,
            'descontoAlunoId' => $this->descontoAlunoId,
            'valor' => $this->valor,
            'valorPago' => $this->valorPago,
            'desconto' => $this->desconto,
            'parcelas' => $this->parcelas,
            'status_pagamento' => $this->status_pagamento,
            'status' => $this->status,
            'dataTransacao' => $this->dataTransacao,
            'dataAtualizacao' => $this->dataAtualizacao,
        ]);

        $query->andFilterWhere(['like', 'cpfAluno', $this->cpfAluno])
            ->andFilterWhere(['like', ' id_transacao', $this-> id_transacao])
            ->andFilterWhere(['like', 'metodoPagamento', $this->metodoPagamento])
            ->andFilterWhere(['like', 'pedido', $this->pedido])
            ->andFilterWhere(['like', 'mensagem_transacao', $this->mensagem_transacao])
            ->andFilterWhere(['like', 'metodo', $this->metodo])
            ->andFilterWhere(['like', 'operadora', $this->operadora])
            ->andFilterWhere(['like', 'operadora_mensagem', $this->operadora_mensagem])
            ->andFilterWhere(['like', 'id_librepag', $this->id_librepag])
            ->andFilterWhere(['like', 'autorizacao_id', $this->autorizacao_id])
            ->andFilterWhere(['like', 'url_autenticacao', $this->url_autenticacao])
            ->andFilterWhere(['like', 'cvv_cartao', $this->cvv_cartao])
            ->andFilterWhere(['like', 'nome_cartao', $this->nome_cartao])
            ->andFilterWhere(['like', 'num_cartao', $this->num_cartao])
            ->andFilterWhere(['like', 'mes_cartao', $this->mes_cartao])
            ->andFilterWhere(['like', 'ano_cartao', $this->ano_cartao])
            ->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'retornoIPAG', $this->retornoIPAG]);

        return $dataProvider;
    }
}
