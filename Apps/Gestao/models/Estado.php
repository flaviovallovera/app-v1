<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estados".
 *
 * @property int $estado
 * @property string|null $sigla
 * @property string|null $nome
 *
 * @property Cidades[] $cidades
 */
class Estado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sigla', 'nome'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'estado' => 'Estado',
            'sigla' => 'Sigla',
            'nome' => 'Nome',
        ];
    }

    /**
     * Gets query for [[Cidades]].
     *
     * @return \yii\db\ActiveQuery|CidadesQuery
     */
    public function getCidades()
    {
        return $this->hasMany(Cidades::className(), ['estado' => 'estado']);
    }

    /**
     * {@inheritdoc}
     * @return EstadoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EstadoQuery(get_called_class());
    }
}
