<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Retorno;

/**
 * RetornoSearch represents the model behind the search form of `app\models\Retorno`.
 */
class RetornoSearch extends Retorno
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['id_transacao', 'num_pedido', 'status_pagamento', 'mensagem_transacao', 'metodo', 'operadora', 'operadora_mensagem', 'id_librepag', 'autorizacao_id', 'url_autenticacao', 'dataAtualizacao'], 'safe'],
            [['valor'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Retorno::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'valor' => $this->valor,
            'dataAtualizacao' => $this->dataAtualizacao,
        ]);

        $query->andFilterWhere(['like', 'id_transacao', $this->id_transacao])
            ->andFilterWhere(['like', 'num_pedido', $this->num_pedido])
            ->andFilterWhere(['like', 'status_pagamento', $this->status_pagamento])
            ->andFilterWhere(['like', 'mensagem_transacao', $this->mensagem_transacao])
            ->andFilterWhere(['like', 'metodo', $this->metodo])
            ->andFilterWhere(['like', 'operadora', $this->operadora])
            ->andFilterWhere(['like', 'operadora_mensagem', $this->operadora_mensagem])
            ->andFilterWhere(['like', 'id_librepag', $this->id_librepag])
            ->andFilterWhere(['like', 'autorizacao_id', $this->autorizacao_id])
            ->andFilterWhere(['like', 'url_autenticacao', $this->url_autenticacao]);

        return $dataProvider;
    }
}
