<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Curso;

/**
 * CursoSearch represents the model behind the search form of `app\models\Curso`.
 */
class CursoSearch extends Curso
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'parcelamentoMaximoCartao', 'tipoCurso'], 'integer'],
            [['nome', 'descricao', 'cargaHoraria', 'Colunatopico1', 'Colunatopico2', 'Colunatopico3', 'Colunatopico4', 'Colunatopico5', 'Colunatopico6', 'Colunatopico7', 'Colunatopico8', 'Colunatopico9', 'Colunatopico10', 'Colunaconteudo1', 'Colunaconteudo2', 'Colunaconteudo3', 'Colunaconteudo4', 'Colunaconteudo5', 'Colunaconteudo6', 'Colunaconteudo7', 'Colunaconteudo8', 'Colunaconteudo9', 'Colunaconteudo10'], 'safe'],
            [['valor', 'valorNoBoleto'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Curso::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'valor' => $this->valor,
            'status' => $this->status,
            'parcelamentoMaximoCartao' => $this->parcelamentoMaximoCartao,
            'valorNoBoleto' => $this->valorNoBoleto,
            'tipoCurso' => $this->tipoCurso,
        ]);

        $query->andFilterWhere(['like', 'nome', $this->nome])
            ->andFilterWhere(['like', 'descricao', $this->descricao])
            ->andFilterWhere(['like', 'cargaHoraria', $this->cargaHoraria])
            ->andFilterWhere(['like', 'Colunatopico1', $this->Colunatopico1])
            ->andFilterWhere(['like', 'Colunatopico2', $this->Colunatopico2])
            ->andFilterWhere(['like', 'Colunatopico3', $this->Colunatopico3])
            ->andFilterWhere(['like', 'Colunatopico4', $this->Colunatopico4])
            ->andFilterWhere(['like', 'Colunatopico5', $this->Colunatopico5])
            ->andFilterWhere(['like', 'Colunatopico6', $this->Colunatopico6])
            ->andFilterWhere(['like', 'Colunatopico7', $this->Colunatopico7])
            ->andFilterWhere(['like', 'Colunatopico8', $this->Colunatopico8])
            ->andFilterWhere(['like', 'Colunatopico9', $this->Colunatopico9])
            ->andFilterWhere(['like', 'Colunatopico10', $this->Colunatopico10])
            ->andFilterWhere(['like', 'Colunaconteudo1', $this->Colunaconteudo1])
            ->andFilterWhere(['like', 'Colunaconteudo2', $this->Colunaconteudo2])
            ->andFilterWhere(['like', 'Colunaconteudo3', $this->Colunaconteudo3])
            ->andFilterWhere(['like', 'Colunaconteudo4', $this->Colunaconteudo4])
            ->andFilterWhere(['like', 'Colunaconteudo5', $this->Colunaconteudo5])
            ->andFilterWhere(['like', 'Colunaconteudo6', $this->Colunaconteudo6])
            ->andFilterWhere(['like', 'Colunaconteudo7', $this->Colunaconteudo7])
            ->andFilterWhere(['like', 'Colunaconteudo8', $this->Colunaconteudo8])
            ->andFilterWhere(['like', 'Colunaconteudo9', $this->Colunaconteudo9])
            ->andFilterWhere(['like', 'Colunaconteudo10', $this->Colunaconteudo10]);

        return $dataProvider;
    }
}
