<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "aluno".
 *
 * @property int $id
 * @property string|null $nome
 * @property string|null $cpf
 * @property string|null $dataNascimento
 * @property string|null $telefone
 * @property string|null $whatsapp
 * @property string|null $email
 * @property int|null $usuario
 * @property string|null $rg
 * @property string|null $numero
 * @property string|null $bairro
 * @property string|null $complemento
 * @property string|null $cep
 * @property int|null $cidade
 * @property string|null $endereco
 * @property string|null $profissao
 * @property string|null $status
 */
class Aluno extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'aluno';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dataNascimento'], 'safe'],
            [['usuario', 'cidade'], 'integer'],
            [['status'], 'string'],
            [['nome', 'cpf', 'telefone', 'whatsapp', 'email'], 'string', 'max' => 45],
            [['rg', 'bairro', 'complemento'], 'string', 'max' => 50],
            [['numero'], 'string', 'max' => 15],
            [['cep'], 'string', 'max' => 20],
            [['endereco', 'profissao'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'cpf' => 'Cpf',
            'dataNascimento' => 'Data Nascimento',
            'telefone' => 'Telefone',
            'whatsapp' => 'Whatsapp',
            'email' => 'Email',
            'usuario' => 'Usuario',
            'rg' => 'Rg',
            'numero' => 'Numero',
            'bairro' => 'Bairro',
            'complemento' => 'Complemento',
            'cep' => 'Cep',
            'cidade' => 'Cidade',
            'endereco' => 'Endereco',
            'profissao' => 'Profissao',
            'status' => 'Status',
        ];
    }

    /**
     * {@inheritdoc}
     * @return AlunoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AlunoQuery(get_called_class());
    }
}
