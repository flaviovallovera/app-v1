<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cidades".
 *
 * @property int $cidade
 * @property string|null $nome
 * @property int|null $estado
 *
 * @property Estado $estado0
 */
class Cidade extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cidades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['estado'], 'integer'],
            [['nome'], 'string', 'max' => 145],
            [['estado'], 'exist', 'skipOnError' => true, 'targetClass' => Estado::className(), 'targetAttribute' => ['estado' => 'estado']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cidade' => 'Cidade',
            'nome' => 'Nome',
            'estado' => 'Estado',
        ];
    }

    /**
     * Gets query for [[Estado0]].
     *
     * @return \yii\db\ActiveQuery|EstadosQuery
     */
    public function getEstado0()
    {
        return $this->hasOne(Estado::className(), ['estado' => 'estado']);
    }

    /**
     * {@inheritdoc}
     * @return CidadeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CidadeQuery(get_called_class());
    }
}
