<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transacao".
 *
 * @property int $id
 * @property int $alunoId
 * @property string $cpfAluno
 * @property int $alunoCursoId
 * @property string $ id_transacao
 * @property int $descontoAlunoId
 * @property float|null $valor
 * @property float|null $valorPago
 * @property float|null $desconto
 * @property int|null $parcelas
 * @property string|null $metodoPagamento
 * @property string|null $pedido
 * @property int|null $status_pagamento
 * @property string|null $mensagem_transacao
 * @property string|null $metodo
 * @property string|null $operadora
 * @property string|null $operadora_mensagem
 * @property string|null $id_librepag
 * @property string|null $autorizacao_id
 * @property string|null $url_autenticacao
 * @property string|null $cvv_cartao
 * @property string|null $nome_cartao
 * @property string|null $num_cartao
 * @property string|null $mes_cartao
 * @property string|null $ano_cartao
 * @property string|null $ip
 * @property int $status 0- não enviado\r\n1- enviado
 * @property string $dataTransacao
 * @property string|null $dataAtualizacao
 * @property string|null $retornoIPAG
 */
class Transacao extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transacao';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alunoId', 'alunoCursoId', 'descontoAlunoId', 'parcelas', 'status_pagamento', 'status'], 'integer'],
            [['valor', 'valorPago', 'desconto'], 'number'],
            [['dataTransacao', 'dataAtualizacao'], 'safe'],
            [['retornoIPAG'], 'string'],
            [['cpfAluno', 'metodoPagamento', 'pedido', 'operadora', 'nome_cartao'], 'string', 'max' => 50],
            [[' id_transacao', 'mensagem_transacao', 'metodo', 'operadora_mensagem', 'id_librepag', 'autorizacao_id', 'url_autenticacao'], 'string', 'max' => 300],
            [['cvv_cartao'], 'string', 'max' => 3],
            [['num_cartao'], 'string', 'max' => 30],
            [['mes_cartao', 'ano_cartao'], 'string', 'max' => 10],
            [['ip'], 'string', 'max' => 60],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alunoId' => 'Aluno ID',
            'cpfAluno' => 'Cpf Aluno',
            'alunoCursoId' => 'Aluno Curso ID',
            ' id_transacao' => 'Id Transacao',
            'descontoAlunoId' => 'Desconto Aluno ID',
            'valor' => 'Valor',
            'valorPago' => 'Valor Pago',
            'desconto' => 'Desconto',
            'parcelas' => 'Parcelas',
            'metodoPagamento' => 'Metodo Pagamento',
            'pedido' => 'Pedido',
            'status_pagamento' => 'Status Pagamento',
            'mensagem_transacao' => 'Mensagem Transacao',
            'metodo' => 'Metodo',
            'operadora' => 'Operadora',
            'operadora_mensagem' => 'Operadora Mensagem',
            'id_librepag' => 'Id Librepag',
            'autorizacao_id' => 'Autorizacao ID',
            'url_autenticacao' => 'Url Autenticacao',
            'cvv_cartao' => 'Cvv Cartao',
            'nome_cartao' => 'Nome Cartao',
            'num_cartao' => 'Num Cartao',
            'mes_cartao' => 'Mes Cartao',
            'ano_cartao' => 'Ano Cartao',
            'ip' => 'Ip',
            'status' => '0- não enviado\\r\\n1- enviado',
            'dataTransacao' => 'Data Transacao',
            'dataAtualizacao' => 'Data Atualizacao',
            'retornoIPAG' => 'Retorno Ipag',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TransacaoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TransacaoQuery(get_called_class());
    }
}
