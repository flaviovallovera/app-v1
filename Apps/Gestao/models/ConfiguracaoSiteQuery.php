<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ConfiguracaoSite]].
 *
 * @see ConfiguracaoSite
 */
class ConfiguracaoSiteQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return ConfiguracaoSite[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ConfiguracaoSite|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
