<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AlunoCurso;

/**
 * AlunoCursoSearch represents the model behind the search form of `app\models\AlunoCurso`.
 */
class AlunoCursoSearch extends AlunoCurso
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'aluno', 'curso', 'possuiCertificado', 'Status', 'turmaId'], 'integer'],
            [['dataCadastro', 'dataFinalizacao'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AlunoCurso::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'aluno' => $this->aluno,
            'dataCadastro' => $this->dataCadastro,
            'curso' => $this->curso,
            'possuiCertificado' => $this->possuiCertificado,
            'Status' => $this->Status,
            'dataFinalizacao' => $this->dataFinalizacao,
            'turmaId' => $this->turmaId,
        ]);

        return $dataProvider;
    }
}
