<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transacaoenderecocobranca".
 *
 * @property int $idTransacao
 * @property string|null $endereco
 * @property string|null $numero
 * @property string|null $complemento
 * @property string|null $bairro
 * @property string|null $cidade
 * @property string|null $estado
 * @property string|null $pais
 * @property string|null $cep
 */
class TransacaoEnderecoCobranca extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transacaoenderecocobranca';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idTransacao'], 'required'],
            [['idTransacao'], 'integer'],
            [['endereco', 'bairro', 'cidade', 'pais'], 'string', 'max' => 50],
            [['numero', 'estado'], 'string', 'max' => 10],
            [['complemento'], 'string', 'max' => 100],
            [['cep'], 'string', 'max' => 12],
            [['idTransacao'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idTransacao' => 'Id Transacao',
            'endereco' => 'Endereco',
            'numero' => 'Numero',
            'complemento' => 'Complemento',
            'bairro' => 'Bairro',
            'cidade' => 'Cidade',
            'estado' => 'Estado',
            'pais' => 'Pais',
            'cep' => 'Cep',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TransacaoEnderecoCobrancaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TransacaoEnderecoCobrancaQuery(get_called_class());
    }
}
