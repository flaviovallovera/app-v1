<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DescontoAluno;

/**
 * DescontoAlunoSearch represents the model behind the search form of `app\models\DescontoAluno`.
 */
class DescontoAlunoSearch extends DescontoAluno
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'aluno', 'status', 'geral'], 'integer'],
            [['percentual', 'valor'], 'number'],
            [['voucher', 'dataLimiteUso', 'cpf'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DescontoAluno::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'aluno' => $this->aluno,
            'percentual' => $this->percentual,
            'valor' => $this->valor,
            'status' => $this->status,
            'dataLimiteUso' => $this->dataLimiteUso,
            'geral' => $this->geral,
        ]);

        $query->andFilterWhere(['like', 'voucher', $this->voucher])
            ->andFilterWhere(['like', 'cpf', $this->cpf]);

        return $dataProvider;
    }
}
