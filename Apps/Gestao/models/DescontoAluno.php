<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "descontoaluno".
 *
 * @property int $id
 * @property int|null $aluno
 * @property float|null $percentual
 * @property float|null $valor
 * @property string|null $voucher
 * @property int|null $status
 * @property string|null $dataLimiteUso
 * @property string|null $cpf
 * @property int|null $geral Caso esteja True, qualquer pessoa com o voucher conseguira o desconto
 */
class DescontoAluno extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'descontoaluno';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['aluno', 'status', 'geral'], 'integer'],
            [['percentual', 'valor'], 'number'],
            [['dataLimiteUso'], 'safe'],
            [['voucher'], 'string', 'max' => 45],
            [['cpf'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'aluno' => 'Aluno',
            'percentual' => 'Percentual',
            'valor' => 'Valor',
            'voucher' => 'Voucher',
            'status' => 'Status',
            'dataLimiteUso' => 'Data Limite Uso',
            'cpf' => 'Cpf',
            'geral' => 'Caso esteja True, qualquer pessoa com o voucher conseguira o desconto',
        ];
    }

    /**
     * {@inheritdoc}
     * @return DescontoAlunoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DescontoAlunoQuery(get_called_class());
    }
}
