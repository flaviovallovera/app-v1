<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AlunoCursoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="aluno-curso-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'aluno') ?>

    <?= $form->field($model, 'dataCadastro') ?>

    <?= $form->field($model, 'curso') ?>

    <?= $form->field($model, 'possuiCertificado') ?>

    <?php // echo $form->field($model, 'Status') ?>

    <?php // echo $form->field($model, 'dataFinalizacao') ?>

    <?php // echo $form->field($model, 'turmaId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
