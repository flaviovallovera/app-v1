<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AlunoCurso */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="aluno-curso-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'aluno')->textInput() ?>

    <?= $form->field($model, 'dataCadastro')->textInput() ?>

    <?= $form->field($model, 'curso')->textInput() ?>

    <?= $form->field($model, 'possuiCertificado')->textInput() ?>

    <?= $form->field($model, 'Status')->textInput() ?>

    <?= $form->field($model, 'dataFinalizacao')->textInput() ?>

    <?= $form->field($model, 'turmaId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
