<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RetornoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="retorno-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_transacao') ?>

    <?= $form->field($model, 'valor') ?>

    <?= $form->field($model, 'num_pedido') ?>

    <?= $form->field($model, 'status_pagamento') ?>

    <?php // echo $form->field($model, 'mensagem_transacao') ?>

    <?php // echo $form->field($model, 'metodo') ?>

    <?php // echo $form->field($model, 'operadora') ?>

    <?php // echo $form->field($model, 'operadora_mensagem') ?>

    <?php // echo $form->field($model, 'id_librepag') ?>

    <?php // echo $form->field($model, 'autorizacao_id') ?>

    <?php // echo $form->field($model, 'url_autenticacao') ?>

    <?php // echo $form->field($model, 'dataAtualizacao') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
