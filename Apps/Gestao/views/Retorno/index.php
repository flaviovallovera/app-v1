<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\RetornoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Retornos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="retorno-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Retorno', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_transacao',
            'valor',
            'num_pedido',
            'status_pagamento',
            //'mensagem_transacao',
            //'metodo',
            //'operadora',
            //'operadora_mensagem',
            //'id_librepag',
            //'autorizacao_id',
            //'url_autenticacao:url',
            //'dataAtualizacao',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
