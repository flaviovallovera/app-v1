<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Retorno */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="retorno-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_transacao')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'valor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'num_pedido')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status_pagamento')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mensagem_transacao')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'metodo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'operadora')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'operadora_mensagem')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_librepag')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'autorizacao_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url_autenticacao')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dataAtualizacao')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
