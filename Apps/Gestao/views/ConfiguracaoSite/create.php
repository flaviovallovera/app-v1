<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ConfiguracaoSite */

$this->title = 'Create Configuracao Site';
$this->params['breadcrumbs'][] = ['label' => 'Configuracao Sites', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="configuracao-site-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
