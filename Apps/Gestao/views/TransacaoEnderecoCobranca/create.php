<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TransacaoEnderecoCobranca */

$this->title = 'Create Transacao Endereco Cobranca';
$this->params['breadcrumbs'][] = ['label' => 'Transacao Endereco Cobrancas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transacao-endereco-cobranca-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
