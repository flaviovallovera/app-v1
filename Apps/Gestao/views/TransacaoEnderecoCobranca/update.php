<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TransacaoEnderecoCobranca */

$this->title = 'Update Transacao Endereco Cobranca: ' . $model->idTransacao;
$this->params['breadcrumbs'][] = ['label' => 'Transacao Endereco Cobrancas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idTransacao, 'url' => ['view', 'id' => $model->idTransacao]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="transacao-endereco-cobranca-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
