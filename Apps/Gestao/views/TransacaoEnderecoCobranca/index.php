<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TransacaoEnderecoCobrancaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transacao Endereco Cobrancas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transacao-endereco-cobranca-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Transacao Endereco Cobranca', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idTransacao',
            'endereco',
            'numero',
            'complemento',
            'bairro',
            //'cidade',
            //'estado',
            //'pais',
            //'cep',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
