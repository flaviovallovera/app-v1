<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TransacaoEnderecoCobranca */

$this->title = $model->idTransacao;
$this->params['breadcrumbs'][] = ['label' => 'Transacao Endereco Cobrancas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="transacao-endereco-cobranca-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idTransacao], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idTransacao], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idTransacao',
            'endereco',
            'numero',
            'complemento',
            'bairro',
            'cidade',
            'estado',
            'pais',
            'cep',
        ],
    ]) ?>

</div>
