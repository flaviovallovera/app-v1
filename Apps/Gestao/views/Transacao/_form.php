<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Transacao */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transacao-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'alunoId')->textInput() ?>

    <?= $form->field($model, 'cpfAluno')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alunoCursoId')->textInput() ?>

    <?= $form->field($model, ' id_transacao')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descontoAlunoId')->textInput() ?>

    <?= $form->field($model, 'valor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'valorPago')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'desconto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'parcelas')->textInput() ?>

    <?= $form->field($model, 'metodoPagamento')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pedido')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status_pagamento')->textInput() ?>

    <?= $form->field($model, 'mensagem_transacao')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'metodo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'operadora')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'operadora_mensagem')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_librepag')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'autorizacao_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url_autenticacao')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cvv_cartao')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nome_cartao')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'num_cartao')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mes_cartao')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ano_cartao')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'dataTransacao')->textInput() ?>

    <?= $form->field($model, 'dataAtualizacao')->textInput() ?>

    <?= $form->field($model, 'retornoIPAG')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
