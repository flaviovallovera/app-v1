<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Transacao */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Transacaos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="transacao-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'alunoId',
            'cpfAluno',
            'alunoCursoId',
            ' id_transacao',
            'descontoAlunoId',
            'valor',
            'valorPago',
            'desconto',
            'parcelas',
            'metodoPagamento',
            'pedido',
            'status_pagamento',
            'mensagem_transacao',
            'metodo',
            'operadora',
            'operadora_mensagem',
            'id_librepag',
            'autorizacao_id',
            'url_autenticacao:url',
            'cvv_cartao',
            'nome_cartao',
            'num_cartao',
            'mes_cartao',
            'ano_cartao',
            'ip',
            'status',
            'dataTransacao',
            'dataAtualizacao',
            'retornoIPAG:ntext',
        ],
    ]) ?>

</div>
