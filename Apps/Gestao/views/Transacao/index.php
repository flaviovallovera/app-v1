<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TransacaoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transacaos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transacao-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Transacao', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'alunoId',
            'cpfAluno',
            'alunoCursoId',
            ' id_transacao',
            //'descontoAlunoId',
            //'valor',
            //'valorPago',
            //'desconto',
            //'parcelas',
            //'metodoPagamento',
            //'pedido',
            //'status_pagamento',
            //'mensagem_transacao',
            //'metodo',
            //'operadora',
            //'operadora_mensagem',
            //'id_librepag',
            //'autorizacao_id',
            //'url_autenticacao:url',
            //'cvv_cartao',
            //'nome_cartao',
            //'num_cartao',
            //'mes_cartao',
            //'ano_cartao',
            //'ip',
            //'status',
            //'dataTransacao',
            //'dataAtualizacao',
            //'retornoIPAG:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
