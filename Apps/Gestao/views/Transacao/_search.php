<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TransacaoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transacao-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'alunoId') ?>

    <?= $form->field($model, 'cpfAluno') ?>

    <?= $form->field($model, 'alunoCursoId') ?>

    <?= $form->field($model, ' id_transacao') ?>

    <?php // echo $form->field($model, 'descontoAlunoId') ?>

    <?php // echo $form->field($model, 'valor') ?>

    <?php // echo $form->field($model, 'valorPago') ?>

    <?php // echo $form->field($model, 'desconto') ?>

    <?php // echo $form->field($model, 'parcelas') ?>

    <?php // echo $form->field($model, 'metodoPagamento') ?>

    <?php // echo $form->field($model, 'pedido') ?>

    <?php // echo $form->field($model, 'status_pagamento') ?>

    <?php // echo $form->field($model, 'mensagem_transacao') ?>

    <?php // echo $form->field($model, 'metodo') ?>

    <?php // echo $form->field($model, 'operadora') ?>

    <?php // echo $form->field($model, 'operadora_mensagem') ?>

    <?php // echo $form->field($model, 'id_librepag') ?>

    <?php // echo $form->field($model, 'autorizacao_id') ?>

    <?php // echo $form->field($model, 'url_autenticacao') ?>

    <?php // echo $form->field($model, 'cvv_cartao') ?>

    <?php // echo $form->field($model, 'nome_cartao') ?>

    <?php // echo $form->field($model, 'num_cartao') ?>

    <?php // echo $form->field($model, 'mes_cartao') ?>

    <?php // echo $form->field($model, 'ano_cartao') ?>

    <?php // echo $form->field($model, 'ip') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'dataTransacao') ?>

    <?php // echo $form->field($model, 'dataAtualizacao') ?>

    <?php // echo $form->field($model, 'retornoIPAG') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
