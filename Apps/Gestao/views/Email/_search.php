<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EmailSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="email-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'Id') ?>

    <?= $form->field($model, 'SmtpServer') ?>

    <?= $form->field($model, 'SmtpPort') ?>

    <?= $form->field($model, 'Username') ?>

    <?= $form->field($model, 'loPassword') ?>

    <?php // echo $form->field($model, 'PassPhrase') ?>

    <?php // echo $form->field($model, 'EnableSsl') ?>

    <?php // echo $form->field($model, 'Subject') ?>

    <?php // echo $form->field($model, 'Body') ?>

    <?php // echo $form->field($model, 'loFrom') ?>

    <?php // echo $form->field($model, 'ReplyTo') ?>

    <?php // echo $form->field($model, 'loTo') ?>

    <?php // echo $form->field($model, 'Cc') ?>

    <?php // echo $form->field($model, 'Cco') ?>

    <?php // echo $form->field($model, 'IsHtml') ?>

    <?php // echo $form->field($model, 'loStatus') ?>

    <?php // echo $form->field($model, 'CreateDate') ?>

    <?php // echo $form->field($model, 'SentDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
