<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Email */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="email-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'SmtpServer')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SmtpPort')->textInput() ?>

    <?= $form->field($model, 'Username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'loPassword')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PassPhrase')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'EnableSsl')->textInput() ?>

    <?= $form->field($model, 'Subject')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Body')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'loFrom')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ReplyTo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'loTo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Cc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Cco')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'IsHtml')->textInput() ?>

    <?= $form->field($model, 'loStatus')->textInput() ?>

    <?= $form->field($model, 'CreateDate')->textInput() ?>

    <?= $form->field($model, 'SentDate')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
