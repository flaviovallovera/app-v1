<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Curso */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cursos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="curso-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nome',
            'descricao:ntext',
            'cargaHoraria',
            'valor',
            'status',
            'Colunatopico1',
            'Colunatopico2',
            'Colunatopico3',
            'Colunatopico4',
            'Colunatopico5',
            'Colunatopico6',
            'Colunatopico7',
            'Colunatopico8',
            'Colunatopico9',
            'Colunatopico10',
            'Colunaconteudo1:ntext',
            'Colunaconteudo2:ntext',
            'Colunaconteudo3:ntext',
            'Colunaconteudo4:ntext',
            'Colunaconteudo5:ntext',
            'Colunaconteudo6:ntext',
            'Colunaconteudo7:ntext',
            'Colunaconteudo8:ntext',
            'Colunaconteudo9:ntext',
            'Colunaconteudo10:ntext',
            'parcelamentoMaximoCartao',
            'valorNoBoleto',
            'tipoCurso',
        ],
    ]) ?>

</div>
