<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CursoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="curso-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nome') ?>

    <?= $form->field($model, 'descricao') ?>

    <?= $form->field($model, 'cargaHoraria') ?>

    <?= $form->field($model, 'valor') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'Colunatopico1') ?>

    <?php // echo $form->field($model, 'Colunatopico2') ?>

    <?php // echo $form->field($model, 'Colunatopico3') ?>

    <?php // echo $form->field($model, 'Colunatopico4') ?>

    <?php // echo $form->field($model, 'Colunatopico5') ?>

    <?php // echo $form->field($model, 'Colunatopico6') ?>

    <?php // echo $form->field($model, 'Colunatopico7') ?>

    <?php // echo $form->field($model, 'Colunatopico8') ?>

    <?php // echo $form->field($model, 'Colunatopico9') ?>

    <?php // echo $form->field($model, 'Colunatopico10') ?>

    <?php // echo $form->field($model, 'Colunaconteudo1') ?>

    <?php // echo $form->field($model, 'Colunaconteudo2') ?>

    <?php // echo $form->field($model, 'Colunaconteudo3') ?>

    <?php // echo $form->field($model, 'Colunaconteudo4') ?>

    <?php // echo $form->field($model, 'Colunaconteudo5') ?>

    <?php // echo $form->field($model, 'Colunaconteudo6') ?>

    <?php // echo $form->field($model, 'Colunaconteudo7') ?>

    <?php // echo $form->field($model, 'Colunaconteudo8') ?>

    <?php // echo $form->field($model, 'Colunaconteudo9') ?>

    <?php // echo $form->field($model, 'Colunaconteudo10') ?>

    <?php // echo $form->field($model, 'parcelamentoMaximoCartao') ?>

    <?php // echo $form->field($model, 'valorNoBoleto') ?>

    <?php // echo $form->field($model, 'tipoCurso') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
