<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Curso */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="curso-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nome')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descricao')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'cargaHoraria')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'valor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'Colunatopico1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Colunatopico2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Colunatopico3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Colunatopico4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Colunatopico5')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Colunatopico6')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Colunatopico7')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Colunatopico8')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Colunatopico9')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Colunatopico10')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Colunaconteudo1')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Colunaconteudo2')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Colunaconteudo3')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Colunaconteudo4')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Colunaconteudo5')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Colunaconteudo6')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Colunaconteudo7')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Colunaconteudo8')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Colunaconteudo9')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Colunaconteudo10')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'parcelamentoMaximoCartao')->textInput() ?>

    <?= $form->field($model, 'valorNoBoleto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipoCurso')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
