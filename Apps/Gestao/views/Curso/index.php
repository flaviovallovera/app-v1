<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CursoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cursos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="curso-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Curso', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nome',
            'descricao:ntext',
            'cargaHoraria',
            'valor',
            //'status',
            //'Colunatopico1',
            //'Colunatopico2',
            //'Colunatopico3',
            //'Colunatopico4',
            //'Colunatopico5',
            //'Colunatopico6',
            //'Colunatopico7',
            //'Colunatopico8',
            //'Colunatopico9',
            //'Colunatopico10',
            //'Colunaconteudo1:ntext',
            //'Colunaconteudo2:ntext',
            //'Colunaconteudo3:ntext',
            //'Colunaconteudo4:ntext',
            //'Colunaconteudo5:ntext',
            //'Colunaconteudo6:ntext',
            //'Colunaconteudo7:ntext',
            //'Colunaconteudo8:ntext',
            //'Colunaconteudo9:ntext',
            //'Colunaconteudo10:ntext',
            //'parcelamentoMaximoCartao',
            //'valorNoBoleto',
            //'tipoCurso',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
