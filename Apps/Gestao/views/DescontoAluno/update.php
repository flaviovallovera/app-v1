<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DescontoAluno */

$this->title = 'Update Desconto Aluno: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Desconto Alunos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="desconto-aluno-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
