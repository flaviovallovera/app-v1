<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\DescontoAlunoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Desconto Alunos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="desconto-aluno-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Desconto Aluno', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'aluno',
            'percentual',
            'valor',
            'voucher',
            //'status',
            //'dataLimiteUso',
            //'cpf',
            //'geral',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
