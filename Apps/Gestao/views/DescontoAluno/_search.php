<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DescontoAlunoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="desconto-aluno-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'aluno') ?>

    <?= $form->field($model, 'percentual') ?>

    <?= $form->field($model, 'valor') ?>

    <?= $form->field($model, 'voucher') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'dataLimiteUso') ?>

    <?php // echo $form->field($model, 'cpf') ?>

    <?php // echo $form->field($model, 'geral') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
