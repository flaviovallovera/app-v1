<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DescontoAluno */

$this->title = 'Create Desconto Aluno';
$this->params['breadcrumbs'][] = ['label' => 'Desconto Alunos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="desconto-aluno-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
